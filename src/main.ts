import { createApp } from "vue";
import { createPinia } from "pinia";
import App from "./App.vue";
import router from "./router";

import BabyLon from "./babylon.config";

const app = createApp(App);

app.use(createPinia());
app.use(router);
app.use(BabyLon);

app.mount("#app");
